#!/bin/bash

screen -X title "**goto**"
screen -X redisplay
screen -X select .

/usr/local/src/goto/goto /tmp/goto.txt
screen -X title "win"

pushd . 2>/dev/null
CMD=`cat /tmp/goto.txt`
$CMD

rm /tmp/goto.txt

echo launching shell
bash -
