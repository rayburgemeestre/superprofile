/*****************************************************************************
*
*                     Ray Burgemeestre
*                     
*                     Basically this is just a tweaked curses example.
*                     (I know it contains a lot of magic numbers and other
*                     hardcoded stuff)
*
*                     If you make some cool improvements, please share them!
*
*
* Compile AIX: 
*  xlC goto.cpp -o goto /usr/lib/libcurses.a
*
* Compile Linux: 
*  g++ goto.cpp -o goto -lncurses
*
* 'goto' alias for your shell:
*  alias goto='. /path/to/goto.sh'
*
* F5 shortcut key for screen works only in shell prompt, add to .screenrc:
*  bindkey -k k5 stuff ". /path/to/goto.sh^M"
*  (The ^M an actual return, using vim in insert mode ctrl+v, ctrl+m.)
*
* F8 shortcut key for screen that invokes goto in a new tab:
*  bindkey -k k8 exec screen /path/to/goto_tab.sh
*
*
* Listing of goto.sh:
* -----------------
* #!/bin/bash
* 
* /path/to/goto /tmp/goto.txt
* pushd . 2>/dev/null
* CMD=`cat /tmp/goto.txt`
* $CMD
* rm /tmp/goto.txt
*
* Listing of goto_tab.sh:
* --------------------------------
* #!/bin/bash
* 
* screen -X title "**goto**"
* screen -X redisplay
* screen -X select .
* 
* /usr/local/src/goto/goto /tmp/goto.txt
* screen -X title "win"
* 
* pushd . 2>/dev/null
* CMD=`cat /tmp/goto.txt`
* $CMD
* 
* rm /tmp/goto.txt
* 
* echo launching shell
* bash -
* ---------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <iostream>
#include <string>
#include <vector>

#include <stdio.h>
#include <curses.h>
#include <string.h>

using std::cout;
using std::cerr;
using std::string;
using std::vector;

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif
#ifndef EXIT_FAILURE
#define EXIT_FAILURE -1
#endif


enum choice_type
{
	type_directory = 1000,
	type_file,
	type_command
};
class choice
{
public:

	choice(int type, string value, string label = "")
	: type_(type),
	  value_(value),
      label_(label)
	{
        if (label_.empty()) label_ = value_;
    }

	int get_type() { return type_; }

	const string get_type_str() const
	{
		switch (type_)
		{
			case type_directory:
				return "directory";
			case type_file:
				return "file";
			case type_command:
				return "command";
		}
	}

	const char *c_str() const { return value_.c_str(); }

    const string get_label() const { return label_; }

private:
	int type_;
	string value_;
	string label_;
};

void print_menu(const vector<choice> &choices, WINDOW *menu_win, int highlight);

int screen_y = 0;
int screen_x = 0;

int main(int argc, char *argv[])
{
	vector<choice> choices;

	if (argc != 2)
	{
		printf("\nUsage: %s <output_file>\n\nParameters:\n"
				"output_file : program writes chosen directory into this destination file.\n\n",
					argv[0]);
		return EXIT_FAILURE;
	}

	choices.push_back(choice(type_directory, "/home/ray/projects/tools"));
	choices.push_back(choice(type_command,   "ssh root@10.2.63.190", "ssh: cdsw"));
    choices.push_back(choice(type_command,   "ssh root@10.2.61.253", "ssh root@10.2.61.253 (rb-ubuntu80dev1)"));
    choices.push_back(choice(type_command,   "ssh root@10.2.63.18", "ssh root@10.2.63.18 (rb-ubuntutrunkdev1)"));
    choices.push_back(choice(type_command,   "ssh root@10.2.61.215", "ssh root@10.2.61.215 (rb-c-01-25-t-c7u2-monitoring )"));
    choices.push_back(choice(type_command,   "ssh root@10.2.63.193", "ssh root@10.2.63.193 (rb-c-06-06-t-c7u2-centos7u2trunk19 )"));
    choices.push_back(choice(type_command,   "ssh root@10.2.63.190", "ssh root@10.2.63.190 (rb-c-06-06-t-c7u2-clouderadatascienceworkbench )"));
    choices.push_back(choice(type_command,   "ssh root@10.2.61.128", "ssh root@10.2.61.128 (rb-c-06-08-b80-dev-c7u2-centos7u2dev80-cl2 )"));


    /*
	choices.push_back(choice(type_directory, "/usr/local/src/"));
	choices.push_back(choice(type_file,      "/usr/local/src/goto/goto.cpp"));
	choices.push_back(choice(type_directory, "/usr/local/src/mongoose/am/"));
	choices.push_back(choice(type_directory, "/usr/local/src/mongoose/am/output/ray-blog-burgemeestre-net"));
	choices.push_back(choice(type_directory, "/var/chroot/lispbot/home/trigen/"));
	choices.push_back(choice(type_file,      "/var/chroot/lispbot/home/trigen/bot.lisp"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts/studiovideo-burgemeestre-net"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts/journeymen-burgemeestre-net"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts/cppse-burgemeestre-net"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts/ray.burgemeestre.net"));
	choices.push_back(choice(type_directory, "/srv/www/vhosts/www.burgemeestre.net"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/cppse-burgemeestre-net.conf"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/journeymen-burgemeestre-net.conf"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/studiovideo-burgemeestre-net.conf"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/ray-burgemeestre-net.conf"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/ray-blog-burgemeestre-net.conf"));
	choices.push_back(choice(type_file,      "/etc/apache2/vhosts.d/www-burgemeestre-net.conf"));
	choices.push_back(choice(type_command,   "/etc/init.d/apache2 restart"));
	choices.push_back(choice(type_command,   "chroot /var/chroot/lispbot"));
	choices.push_back(choice(type_command,   "top"));
	choices.push_back(choice(type_command,   "screen -x"));
    */


	int highlight = 1;
	int choice = 0;

	initscr();

	start_color();
	init_pair(1, COLOR_WHITE, COLOR_RED);

	getmaxyx(stdscr, screen_y, screen_x);

	int window_x = screen_x - 10;
	int window_y = screen_y - 5;
	int startx = 2;
	int starty = 2;

	noecho();
	cbreak();	// Line buffering disabled. pass on everything 
	
		
	WINDOW *menu_win = newwin(window_y, window_x, starty, startx);
	mvprintw(0, 0, "Use arrow keys to go up and down, or vim keys j and k. Press enter to select a choice. ctrl+c to exit.");
	keypad(menu_win, TRUE);
	refresh();
	do
	{
		clear();
		mvprintw(0, 0, "Use arrow keys to go up and down, or vim keys j and k. Press enter to select a choice. ctrl+c to exit.");
		print_menu(choices, menu_win, highlight);
		if(choice != 0)	// User did a choice come out of the infinite loop
			break;

		int c = wgetch(menu_win);
		switch(c)
		{
			case KEY_UP:
			case 'k':
				if(highlight == 1)
					highlight = choices.size();
				else
					--highlight;
				break;
			case KEY_DOWN:
			case 'j':
				if(highlight == choices.size())
					highlight = 1;
				else 
					++highlight;
				break;
			case 10:
				choice = highlight;
				break;
			case KEY_RESIZE:
			{
				int old_screen_x = screen_x, old_screen_y = screen_y;
				getmaxyx(stdscr, screen_y, screen_x);
				window_x = screen_x - 10;
				window_y = screen_y - 5;

				endwin();
				menu_win = newwin(window_y, window_x, starty, startx);
				break;
			}
			default:
				mvprintw(screen_y - 2, 0, "Charcter pressed is = %3d Hopefully it can be printed as '%c' (Use ctrl+c to exit)", c, c);
				refresh();
				break;
		}
	} while (true);	
	//mvprintw(23, 0, "You chose choice %d with choice string %s\n", choice, choices[choice - 1]);
	clrtoeol();
	refresh();
	endwin();
	//printf("You chose choice %d with choice string %s\n", choice, choices[choice - 1]);
	//fprintf(stderr, "DIRECTORY %s\n", choices[choice - 1]);

	FILE *fd = fopen(argv[1], "w");
	switch (choices.at(choice - 1).get_type())
	{
		case type_directory:
			fprintf(fd, "cd %s", choices.at(choice - 1).c_str());
			break;
		case type_file:
			fprintf(fd, "vim %s", choices.at(choice - 1).c_str());
			break;
		case type_command:
			fprintf(fd, "%s", choices.at(choice - 1).c_str());
			break;
	}
	fflush(fd);
	fclose(fd);

	return EXIT_SUCCESS;
}


void print_menu(const vector<choice> &choices, WINDOW *menu_win, int highlight)
{
	int x = 2, y = 2;	

	box(menu_win, 0, 0);
	int counter = 0;


	wattron(menu_win, COLOR_PAIR(1));
	mvwprintw(menu_win, y - 1, x - 1, "%-*s", screen_x - 12, " Listing");
	wattroff(menu_win, COLOR_PAIR(1));

	for(vector<choice>::const_iterator i=choices.begin(); i!=choices.end(); ++i)
	{	
		const choice &ch(*i);

		if((highlight - 1) == counter) // High light the present choice
		{	
			wattron(menu_win, A_REVERSE); 
			mvwprintw(menu_win, y, x, "%-10s %s", ch.get_type_str().c_str(), ch.get_label().c_str());
			wattroff(menu_win, A_REVERSE);

			char path[256 + 1] = {0x00};
			char cmd[256 + 1] = {0x00};
			char title[80+1] = {0x00};
			int num = 9;
			switch (const_cast<choice &>(ch).get_type())
			{
				case type_file:
					sprintf(cmd, "cat \"%s\"| head -n %d 2>&1", ch.c_str(), num);
					strcpy(title, "Beginning of file contents:");
					break;
				case type_directory:
					sprintf(cmd, "ls -altr \"%s\" | tail -%d 2>&1", ch.c_str(), num);
					strcpy(title, "Most recently changed files in directory:");
					break;
				case type_command:
					memset(cmd, 0x00, sizeof(cmd));
					strcpy(title, "");
					break;
			}
			FILE *fp = popen(cmd, "r");
			if (fp == NULL)
				break; // ignore errors

			for (int j=strlen(title); j<sizeof(title); ++j)
				title[j] = ' ';
			title[sizeof(title) - 1] = 0;

			wattron(menu_win, COLOR_PAIR(1));
			mvwprintw(menu_win, screen_y - 18, x - 1, "%-*s", screen_x - 12, " Details");
			wattroff(menu_win, COLOR_PAIR(1));

			mvwprintw(menu_win, screen_y - 17, x, title); // I want that annoying cursor out of the way :P
			for (int i=0; i<num; ++i)
			{
				if (fgets(path, sizeof(path) - 1, fp) == NULL)
				{
					// ignore failures
				}

				for (int j=0; j<strlen(path); ++j)
					if (path[j] == 10 || path[j] == 13)
						path[j] = 0;
				for (int j=strlen(path); j<sizeof(path); ++j)
					path[j] = ' ';
				path[sizeof(path) - 1] = 0;

				mvwprintw(menu_win, screen_y - 15 + i, 5, string(path).substr(0, screen_x - 20).c_str()); // I want that annoying cursor out of the way :P
			}


		}
		else
			mvwprintw(menu_win, y, x, "%-10s %s", ch.get_type_str().c_str(), ch.c_str());
		++y;
		counter++;
	}
	
	mvwprintw(menu_win, y + 1, x, ""); // I want that annoying cursor out of the way :P

	wrefresh(menu_win);
}
