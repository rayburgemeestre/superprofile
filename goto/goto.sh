#!/bin/bash

/usr/local/src/goto/goto /tmp/goto.txt
pushd . 2>/dev/null
CMD=`cat /tmp/goto.txt`
$CMD
pwd
rm /tmp/goto.txt
