import os
from os.path import expanduser
import shutil
import sys

home = expanduser("~")

lookup = {
    'notes' : '{}/notes'.format(home),
    '3dprinting' : '{}/3dprinting'.format(home),
    'hidden' : None,
    'projects' : '{}/projects'.format(home),
    'movies' : '{}/movies'.format(home),
    'pictures' : '{}/Pictures'.format(home),
    'scripts' : '{}/scripts'.format(home),
    'documents' : '{}/Documents'.format(home),
    'music' : '{}/music'.format(home),
    'delete' : '{}/delete'.format(home),
    'personal' : '{}/.personal'.format(home),
    'backups' : '{}/backups'.format(home),
    'graphics' : '{}/graphics'.format(home),
    'home' : '{}/'.format(home),
}

todo_move = []
todo_delete = []

for filename in os.listdir(home):
    if filename.startswith("ranger_") and filename.endswith(".txt"):
        with open(os.path.join(home, filename), 'r') as f:
            files = f.read().strip().split("\n")
            files = set(files)
            what = filename.replace('ranger_queued_', '').replace('.txt', '')
            dest = lookup[what]
            if dest:
                for file in files:
                    if what == 'delete':
                        if os.path.exists(file):
                            todo_delete.append({
                                'src' : file,
                                'dst' : dest
                            })
                    else:
                        if os.path.exists(file):
                            todo_move.append({
                                'src' : file,
                                'dst' : dest
                            })

def mkdirp(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory) 


def create_moves(todo_list):
    moves = []
    for task in todo_list:
        mkdirp(task['dst'])
        counter = 0
        while True:
            dst_name = os.path.basename(task['src'])
            if counter > 0:
                dst_name = "[dupe_{}]{}".format(counter, dst_name)
            if os.path.exists(os.path.join(task['dst'], dst_name)):
                print("Unfortunately already exists: {} in {}".format(task['src'], dst_name))
                counter += 1
            else:
                print("OK: {} in {}".format(task['src'], dst_name))
                moves.append({
                    'src' : task['src'],
                    'dst' : os.path.join(task['dst'], dst_name)
                })
                break
    return moves

moves = create_moves(todo_move)

# Execute moves
for move in moves:
    if os.path.exists(move['src']):
        print(move)
        try:
            shutil.move(move['src'], move['dst'])
        except Exception as e:
            print(e)

# write back what is left
def update_queues():
    all_non_delete_queues_empty = True
    for filename in os.listdir(home):
        if filename.startswith("ranger_") and filename.endswith(".txt"):
            with open(os.path.join(home, filename), 'r') as fd:
                files = fd.read().strip().split("\n")
                files = set(files)
                what = filename.replace('ranger_queued_', '').replace('.txt', '')
                dest = lookup[what]
                if dest:
                    print("filename: {}".format(filename))
                    print(" files: {}".format([f for f in files if os.path.exists(f)]))
                    fd.close()
                    with open(os.path.join(home, filename), 'w') as fd:
                        for f in [f for f in files if os.path.exists(f)]:
                            fd.write(f + "\n")
                            if what != 'delete':
                                all_non_delete_queues_empty = False
    return all_non_delete_queues_empty

if update_queues():
    print("Proceed with deleting..")

    moves = create_moves(todo_delete)

    for move in moves:
        if os.path.exists(move['src']):
            print("Deleting! {}".format(move))
            try:
                shutil.move(move['src'], move['dst'])
            except Exception as e:
                print(e)

    update_queues()
