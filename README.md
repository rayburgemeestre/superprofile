## INSTALL

* `git submodule update --init --recursive`, run this first, to initialize this repo
* `execute_once.sh`, this deploys stuff in the `once` folder, including proper API keys for chromium to work correctly.
* `bash install_tools.sh`, deploy some commonly used packages I use, Run `lxappearance` to fix the cursor theme!
* `bash install_links.sh`, deploy symlinks in home folder!

This last command will backup existing dot files.

Deploy chromium browser manually. If you want GPU acceleration support etc., you may require some kind of ppa.


## NOTES

Adding submodules:

preferably use https:// instead of git@..

    git submodule add https://github.com/dhruvasagar/vim-table-mode .vim/bundle/vim-table-mode
    git submodule add git@github.com:morhetz/gruvbox.git .vim/bundle/gruvbox
    git submodule add git@github.com:morhetz/gruvbox.git .vim/bundle/gruvbox
    git submodule add https://github.com/chriskempson/vim-tomorrow-theme .vim/bundle/vim-tomorrow-theme
    git submodule add https://github.com/nvie/vim-flake8 .vim/bundle/vim-flake8
    git submodule add https://github.com/tmhedberg/SimpylFold.git .vim/bundle/SimpylFold
    git submodule add https://github.com/tpope/vim-fugitive .vim/bundle/fugitive
    git submodule add https://github.com/Valloric/YouCompleteMe .vim/bundle/youcompleteme
    git submodule add https://github.com/rust-lang/rust.vim .vim/bundle/rust
    git submodule init

check .gitmodules

    git submodule add https://github.com/rayburgemeestre/light light
