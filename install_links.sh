#!/bin/ksh93

mkdir -p ~/.i3 2>/dev/null
mkdir -p /usr/share/fonts/truetype/menlo 2>/dev/null
mkdir -p ~/.fonts 2>/dev/null

mkdir -p /usr/share/fonts/truetype/menlo 2>/dev/null
cp -prv fonts/menlo/* /usr/share/fonts/truetype/menlo/
cp -prv fonts/Monaco_Linux-Powerline.ttf ~/.fonts/

function link
{
    mv -v ~/$1 ~/$1.bak 2>/dev/null
    ln -s -v ${PWD}/dot-files/$1 ~/$1
}
link .kshrc
link .screenrc
link .vimrc
link .vim
link .i3
link .gitconfig
link .tigrc
link .Xdefaults
link .Xresources
link .Xdefaults
link .bin
link bin
link .tmux.conf
link .urxvt

mkdir -p ~/.config
link .config/ranger
link .config/fish

if ! [[ -f ~/.kshrc.local ]]; then
    cp -prv .kshrc.local-dist ~/.kshrc.local
fi

if ! [[ -d ~/.ksh_functions ]]; then
    tar -zxvf kshfun.tar.gz
    mv fun ~/.ksh_functions
fi


cd dot-files/.vim
git submodule update --init --recursive
