#!/bin/bash

ME=$USER

if [[ "$ME" = "root" ]]; then
    echo sorry do not execute this script as root, sudo is in place where needed..
    exit 1
fi

if [[ "$1" = "" ]]; then
    echo please specify which once directory you wish to process
    exit 1
fi

function handle_func
{
    while read line; do
        typeset file=${line//.*\/}
        typeset once_file=".once-done/$file"
        if [[ -f $once_file ]]; then
            echo $file is already executed, skipping
            continue
        fi
        $SHELL $line
        touch $once_file
    done < /dev/stdin
}

find $1 -type f | handle_func

