" Plugin for inserting "poor man"'s debug print statements
"
" Maintainer: Ray Burgemeestre
" Last Change: 2015 Aug 11
"
" INSTALL
"  1. Put debug.vim in your plugin directory (~/.vim/ftplugin/php/)???
"
"  2. You might want to add the following keyboard mappings to your .vimrc:
"
"       map <F3> <Esc>:InsertDebugLine1<Cr>
"       map <F4> <Esc>:InsertDebugLine2<Cr>
"

command! InsertDebugLine1 call <SID>InsertDebugLineCvlog()
command! InsertDebugLine2 call <SID>InsertDebugLineStdErr()

function! s:InsertDebugLineStdErr()
    " std::cout << ".." << std::endl;
	normal! yyP0
    silent! :s/"/\\"/g
	normal! 0
    normal! istd::cerr << __FILE__ << "@" << __LINE__ << "
    normal! $a" << std::endl;
    normal! 0jjzz
endfunction

function! s:InsertDebugLineCvlog()
    " CVLOG_DEBUG(__FILE__ << "@" << __LINE__ << " ..");
	normal! yyP0
    silent! :s/"/\\"/g
	normal! 0
    normal! iCVLOG_DEBUG(__FILE__ << "@" << __LINE__ << " 
    normal! $a");
    normal! 0jjzz
endfunction

" vim:ft=vim:foldmethod=marker:nowrap:tabstop=4:shiftwidth=4
