#!/bin/bash

for sink in $(pacmd list-sinks | grep -e 'name:' | sed 's/.*<\(.*\).*>/\1/g'); do
    if [[ $1 == "inc" ]]; then
        pactl set-sink-volume $sink +2%
    elif [[ $1 == "dec" ]]; then
        pactl set-sink-volume $sink -2%
    elif [[ $1 == "mute" ]]; then
        pactl set-sink-mute $sink true
    elif [[ $1 == "toggle" ]]; then
        pactl set-sink-mute $sink toggle
    fi
done

