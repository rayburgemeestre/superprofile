#!/bin/ksh93

for sink in $(pacmd list-sinks | grep -e 'name:' | sed 's/.*<\(.*\).*>/\1/g'); do
    pactl set-sink-mute $sink true
done

bash /home/trigen/clear.sh

killall -9 mpv ffmpeg

