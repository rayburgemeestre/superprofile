<?php
$work = false;
//$start = !$work ? '10:30' : '10:30' ; //system("timeout 5s ssh -i /home/trigen/.ssh/id_rsa pi@192.168.2.126 \"stat /tmp/go_home.lock |grep Modify:|cut -d ' ' -f 3|cut -d '.' -f 1\"");
$start = '10:30';

$startTime   = strtotime('today ' . $start);
$currentTime = strtotime('now');
$stopTime    = strtotime('today ' . $start .' + 8 hours + 30 minutes');

$dt = new DateTime(date('Y-m-d H:i:s', $stopTime));
$diff = $dt->diff(new DateTime(date('Y-m-d H:i:s', $currentTime)));
$diffTotal = $dt->diff(new DateTime(date('Y-m-d H:i:s', $startTime)));

$percentage =  ($diff->h * 60 + $diff->i) / ($diffTotal->h * 60 + $diffTotal->i);

$bar = '';
$size = 25;
if (false)
for ($i=0; $i<$size; $i++) {
    $bar .= ($i / $size > (1 - $percentage)) ? '░' : '▓';
}
else
    $bar .= $diff->h . ":" . $diff->i . " @work | ";

$done = ($currentTime > $stopTime);

$memory_usage = 'mem free: ';
$memory_usage .= system("echo $(free -m -h|grep Mem)|cut -d ' ' -f 4");
#$memory_usage .= '/';
#$memory_usage .= system("echo $(free -m -h|grep Mem)|cut -d ' ' -f 6");
#$memory_usage .= '/';
#$memory_usage .= system("echo $(free -m -h|grep Mem)|cut -d ' ' -f 2");

$sendmsg = system('tail -n 1 /tmp/sendmsg.log');

if ($work) {
    printf("[%s] 😀 | %s - %s %s %s %s= %02d:%02d ⁕ ", $sendmsg, $memory_usage, date('H:i', $startTime), $bar, date("H:i", $stopTime),
        ($done) ? 'GO HOME! ' : '', $diff->h, $diff->i);
} else {
    print $bar . $memory_usage;
}
?>
