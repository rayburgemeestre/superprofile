#!/bin/bash

declare -i ID
ID=`xinput list | grep -i 'TouchPad' | sed 's/.*id=//' | sed 's/\t.*//'`
declare -i STATE
STATE=`xinput list-props $ID|grep 'Device Enabled'|awk '{print $4}'`
if [ $STATE -eq 1 ]
then
    xinput disable $ID
    echo "Touchpad disabled."
else
    xinput enable $ID
    echo "Touchpad enabled."
fi

