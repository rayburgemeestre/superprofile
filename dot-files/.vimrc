set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

" The monokai color scheme.
Plugin 'filfirst/Monota'

" The project source tree browser.
Plugin 'scrooloose/nerdtree'

" The enhanced editor status bar.
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" The enhanced C++ syntax highlighting.
Plugin 'octol/vim-cpp-enhanced-highlight'

" The auto-complete module.
Plugin 'Valloric/YouCompleteMe'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" ---------- Monokai color scheme ----------
syntax on
colorscheme Monota

" ---------- General Settings ----------
set backspace=indent,eol,start

syntax enable

" Show line numbers
set number

" Highlight matching brace
set showmatch

" Highlight all search results
set hlsearch

" Highlight the current cursor line
set cursorline

" Highlight the 80 columns margin.
set colorcolumn=80

" Trim the trailing white space on save.
autocmd BufWritePre <buffer> :%s/\s\+$//e

" ---------- Indentation ----------
" Use spaces instead of tabs
set expandtab

" Number of spaces that a <TAB> in the file counts for
set tabstop=4

" Number of auto-indent spaces
set shiftwidth=4
set autoindent

" ---------- Folding ----------
set foldenable
set foldmethod=syntax

" Do not fold the code by default
set foldlevel=10000

" ---------- NerdTree Project Browser ----------
nnoremap <C-n> :NERDTreeToggle<CR>

let NERDTreeShowHidden=1
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif


" ---------- Enhanced C++ syntax highlighting ----------
let g:cpp_class_scope_highlight=1
let g:cpp_concepts_highlight=1
let g:cpp_experimental_simple_template_highlight=1


" ---------- YCM Auto Complete ----------
nnoremap <F12> :YcmCompleter GoTo<CR>

"let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_collect_identifiers_from_tags_files = 1

" --- original ---
"
execute pathogen#infect()

cmap w!! w !sudo tee > /dev/null %

set term=screen-256color

set bg=dark
set ve=block
set tabstop=4
set shiftwidth=4

let hostname = substitute(system('hostname'), '\n', '', '')

" Bright Computing
if hostname == "ray-centos-pc"
set tabstop=2
set shiftwidth=2
endif

set smartindent
set expandtab


"function! NumberToggle()
"  if(&relativenumber == 1)
"    set number
"  else
"    set relativenumber
"  endif
"endfunc
"
"nnoremap <F12> :call NumberToggle()<cr>
"call NumberToggle()
":au FocusLost * :set number
":au FocusGained * :set relativenumber


filetype plugin indent on
silent source ~/.vim/ftplugin/php/phpfolding.vim

map <F5> <Esc>:EnableFastPHPFolds<Cr>
map <F6> <Esc>:EnablePHPFolds<Cr>
map <F7> <Esc>:DisablePHPFolds<Cr>

silent source ~/.vim/autoload/debug.vim
map <F3> <Esc>:InsertDebugLine1<Cr>
map <F4> <Esc>:InsertDebugLine2<Cr>

set nocompatible
syntax on
"set mouse=a " Note: if you use this and want to copy from terminal, use SHIFT when selecting :-)
set mouse=n " I  prefer n to only use it for moving the mouse cursor, not selecting etc.
set mousehide
scriptencoding utf-8
"if has('clipboard')
"	if has('unnamedplus')  " When possible use + register for copy-paste
"		set clipboard=unnamed,unnamedplus
"	else         " On mac and Windows, use * register for copy-paste
"		set clipboard=unnamed
"	endif
"endif

set shortmess+=filmnrxoOtT          " Abbrev. of messages (avoids 'hit enter')
set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
set virtualedit=onemore             " Allow for cursor beyond last character
set history=1000                    " Store a ton of history (default is 20)
"set spell                           " Spell checking on
set hidden                          " Allow buffer switching without saving
set iskeyword-=.                    " '.' is an end of word designator
set iskeyword-=#                    " '#' is an end of word designator
set iskeyword-=-                    " '-' is an end of word designator
set backup                  " Backups are nice ...
if has('persistent_undo')
    set undofile                " So is persistent undo ...
    set undolevels=1000         " Maximum number of changes that can be undone
    set undoreload=10000        " Maximum number lines to save for undo on a buffer reload
endif
"    set tabpagemax=15               " Only show 15 tabs
set showmode                    " Display the current mode
"set cursorline                  " Highlight current line
"
"    highlight clear SignColumn      " SignColumn should match background
"    highlight clear LineNr          " Current line number row will have same background color in relative mode
"    "highlight clear CursorLineNr    " Remove highlight color from current line number
"
"    if has('cmdline_info')
set ruler                   " Show the ruler
set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
set showcmd                 " Show partial commands in status line and
"                                    " Selected characters/lines in visual mode
"    endif
"
if has('statusline')
set laststatus=2

" Broken down into easily includeable segments
set statusline=%<%f\                     " Filename
set statusline+=%w%h%m%r                 " Options
"if !exists('g:override_spf13_bundles')
"    set statusline+=%{fugitive#statusline()} " Git Hotness
"endif
set statusline+=\ [%{&ff}/%Y]            " Filetype
set statusline+=\ [%{getcwd()}]          " Current dir
set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
endif
"
"    set backspace=indent,eol,start  " Backspace for dummies
"    set linespace=0                 " No extra spaces between rows
set number                      " Line numbers on
set showmatch                   " Show matching brackets/parenthesis
set incsearch                   " Find as you type search
set hlsearch                    " Highlight search terms
"    set winminheight=0              " Windows can be 0 line high
"    set ignorecase                  " Case insensitive search
"    set smartcase                   " Case sensitive when uc present
set wildmenu                    " Show list instead of just completing
"set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.
set wildmode=list:longest

"    set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
"    set scrolljump=5                " Lines to scroll when cursor leaves screen
"    set scrolloff=3                 " Minimum lines to keep above and below cursor
"    set foldenable                  " Auto fold code

set list
set listchars=tab:!·,trail:·,eol:¬


let g:solarized_termcolors=256
set t_Co=256

syntax enable
"set background=light
"set background=dark
colorscheme solarized
"colorscheme gruvbox
"set bg=dark
"colorscheme Tomorrow-Night-Bright

" Excellent theme, but the LineNumbering is a bit off, so set that to grey.
colorscheme Tomorrow-Night-Blue
highlight LineNr ctermfg=grey
hi Visual term=reverse cterm=reverse guibg=Grey

" gvim would be: highlight LineNr guifg=#050505

" enable this to visualize > 120 chars..
"highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"match OverLength /\%121v.\+/

" mkdir ~/.vimundo
" mkdir ~/.vimswap
" mkdir ~/.vimbak
set dir=~/.vimswap//,/var/tmp//,/tmp//,.
set undodir=~/.vimundo//,/var/tmp//,/tmp//,.
set backupdir=~/.vimbak//,/var/tmp//,/tmp//,.

"map <F8> <Esc>:!cmake -DSTATIC=1 . && make -j 8 starcry && ./starcry --gui -s input/sgi_elems.js<Cr>
"map <F8> <Esc>:!cmake -DSTATIC=1 . && make -j 8 starcry && cat sgi.log | ./starcry --stdin --gui -s input/sgi.js<Cr>

command! DiffAgainstFileOnDisk call DiffAgainstFileOnDisk()

function! DiffAgainstFileOnDisk()
  :w! /tmp/working_copy
  exec "!echo changes against file on disk.. ; diff % /tmp/working_copy"
endfunction
