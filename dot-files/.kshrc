################################################################################
#       _____ __ __  ____   ___  ____   ____     ___    ___  ____              #
#      / ___/|  |  ||    \ /  _]|    \ |    \   /  _]  /  _]|    \             #
#     (   \_ |  |  ||  o  )  [_ |  D  )|  o  ) /  [_  /  [_ |  D  )            #
#      \__  ||  |  ||   _/    _]|    / |     ||    _]|    _]|    /             #
#      /  \ ||  :  ||  | |   [_ |    \ |  O  ||   [_ |   [_ |    \             #
#      \    ||     ||  | |     ||  .  \|     ||     ||     ||  .  \            #
#       \___| \__,_||__| |_____||__|\_||_____||_____||_____||__|\_|            #
#                                                                              #
#                             KSH profile                                      #
################################################################################

set -o vi

################################################################################
# Fix prompt and include git branch where appropriate                          #
################################################################################

export HOST=`hostname`

export LS_COLORS=

alias kshps1git="git branch 2>&1 |egrep \"^\\\*\" | sed 's/^* \\(.*\\)/[\\1]/g'"

# one line version:
# this one works for bash

if test "$UID" = 0 ; then
    red=$(tput setaf 1 2>/dev/null)
    reset=$(tput sgr0 2>/dev/null)
    PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\`kshps1git\`\[$red\]#\[$reset\] "
else
    # but this works
#   0 – Black
#   1 – Red
#   2 – Green
#   3 – Yellow
#   4 – Blue
#   5 – Magenta
#   6 – Cyan
#   7 – White
#
    #red=$(tput setaf 6)
    red=$(tput setaf 15 2>/dev/null)
    bold=$(tput bold 2>/dev/null)
    reset=$(tput sgr0 2>/dev/null)
    #PS1='\[$red\][\W]\$\[$reset\] '
    PS1="\[$red\]\[$bold\]\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\`kshps1git\`>\[$reset\] "

    PS1="\[$red\]\[$bold\]\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\`kshps1git\`>\[$reset\]\033]11;#00FF00\007\033]10;black\007 "


    # The following coloring trick screws up history, also control+u sometimes...
    # i.e., type "systemctl restart network-manager; ", followed by ctrl+u
    # PS1="[33m$PS1[0m"
    # SO DONT USE THAT, USE TPUT
    #EDIT: actually the fix is with the \[ \]'s, ; http://askubuntu.com/questions/111840/ps1-problem-messing-up-cli
fi

PROMPT_COMMAND=prompt_command
PREVIOUS_COMMAND=""
TOGGLE=0
toggle() {
    if [[ $TOGGLE -eq 1 ]]; then TOGGLE=0; else TOGGLE=1; fi
}
prompt_command() {
	local EXIT="$?"
    local last_cmd="$(history | grep -v history | tail -n 1)"
    if [[ $TOGGLE -eq 1 ]]; then
        if [[ $PREVIOUS_COMMAND == $last_cmd ]]; then
            local exit_status='\[\033]11;#5955E0\007\033]10;white\007\]'
        elif [ $EXIT == 0 ]; then
            local exit_status='\[\033]11;#00FF00\007\033]10;black\007\]'
        else
            local exit_status='\[\033]11;#FF0000\007\033]10;black\007\]'
        fi
	fi
    PREVIOUS_COMMAND="$last_cmd"
	PS1="\[$exit_status\]\[$red\]\[$bold\]\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\`kshps1git\`>\[$reset\] "
}

# this one works only in pure ksh afaik.. but not sure of that either.
# the PWD is causing some strange issues when navigating previous commands

#PS1="${USER}@${HOST}:"'${PWD}'"\`kshps1git\`> "
# alternative colors (copy one in your .kshrc.local for example)
# blue: PS1="[34m$PS1[0m"
# cyan: PS1="[36m$PS1[0m"
# production
# red: PS1="[31m$PS1[0m"
# red++: PS1="[0m[41m$PS1[0m[31m"

# two line version:

function extend_ps1 {
    PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\`kshps1git\`>
"
    #PS1="[31m$PS1[0m"
    #PS1="[1;36m$PS1[0m"
}

################################################################################
# Share history amongst shells                                                 #
################################################################################

# Note that if you are going to use this, make sure your base ".ksh_history" is
# a valid history file.
# Apparently histfiles get truncated (regardless of the sizes specified) in
# case they do not start with '0x81 0x01'
#
# hexdump -C $HISTFILE|head -n 1
# 00000000  81 01 63 64 20 6f 70 65  6e 76 70 6e 2f 0a 73 75  |..cd openvpn/.su|
#
# I now simply prefix it in function hist_commit

if ! [[ -f "$HOME/.ksh_history" ]]; then touch ~/.ksh_history; fi
if ! [[ -d "$HOME/.ksh_history_files" ]]; then mkdir ~/.ksh_history_files; fi

DEFHISTFILE=~/.ksh_history
NEWHISTFILE=~/.ksh_history_files/$$
function uniq_with_memory
{
    typeset -A lines
    while read line; do
        if [[ ! ${lines[$line]} ]]; then
            echo $line;
            lines[$line]=true
        fi
    done < /dev/stdin
}
function hist_commit
{
    (cd ~

     LC_CTYPE=C
     LANG=C

     # the following doesn't work on OSX, apparently sed doesn't work exactly the same as Linux.
     # so I found an alternative solution on <http://stackoverflow.com/questions/21909228/why-cant-sed-replace-0x24-byte-in-binary-file>
     ((# linux: (cat .ksh_history .ksh_history_files/* | sed 's/\x81\x01//g')
       # osx:
       (cat ~/.ksh_history | hexdump -ve '1/1 "%.2X"' | sed 's/8101//g' | xxd -r -p)
      ) | tr '\0' '\2' | uniq_with_memory |
          tr '\2' '\0' > .ksh_history.new)

     # only keep the most recent 10.000 history items
     mv -f .ksh_history .ksh_history.old && \
     (printf "\x81\x01" && tail -n 10000 .ksh_history.new) > .ksh_history

     # start the lsof check + removal in background, as this can be a bit slow
     (for file in ~/.ksh_history_files/*; do
          if [[ $(lsof $file 2>/dev/null|wc -l) -eq 0 ]]; then
              rm -rf $file;
          fi
      done)&)
}
function hist_load
{
    (cd ~
     hist_commit
     cp -fprv $DEFHISTFILE $NEWHISTFILE
    )
}

if [[ $KSH_VERSION ]]; then
    hist_load

    export HISTFILESIZE=1000000000
    export HISTSIZE=50000
    export HISTFILE=$NEWHISTFILE
fi

################################################################################
# Enable "dirs"                                                                #
################################################################################

if [[ $KSH_VERSION ]]; then
    if [[ -d "/usr/share/ksh/functions/dirs" ]]; then
        . /usr/share/ksh/functions/dirs
    elif [[ -d "$HOME/.ksh_functions" ]]; then
        . ~/.ksh_functions/pushd
        . ~/.ksh_functions/dirs
    else
        printf "dirs cannot be loaded\n"
    fi

    function _cd_with_fix_for_home {
        if [[ "$*" = "" ]]; then
            _cd $HOME
        else
            print "$*" >/tmp/.lastDir
            cd "$*"
        fi
    }
    alias cd=_cd_with_fix_for_home
    if [[ -f "/tmp/.lastDir" ]]; then
        cd "$(cat /tmp/.lastDir)"
    fi
elif [[ $BASH_VERSION ]]; then
    echo -n "" # TODO: find equivalent bash implementation for "dirs" feature
fi

################################################################################
# Colorize                                                                     #
################################################################################

function colorize
{
    alias ls='ls --color'
    alias grep='grep --color'
}
function decolorize
{
    unalias ls
    unalias grep
    echo Decolorized. use \`colorize\' to re-enable colors.
}

colorize

################################################################################
# Internet shortcuts
################################################################################

function rawurlencode
{
    typeset string="${*}"
    typeset strlen=${#string}
    typeset encoded=""

    for (( pos=0 ; pos<strlen ; pos++ )); do
    c=${string:$pos:1}
    case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               o=$(printf '%%%02x' "'$c")
    esac
    encoded+="${o}"
    done
    print "${encoded}"
}

function rawurldecode
{
    printf $(printf '%b' "${1//%/\\x}")
}

#print $(rawurlencode "C++")     # --> C%2b%2b
#print $(rawurldecode "C%2b%2b") # --> C++

function i_ {
    chromium-browser "$(rawurlencode $*)"
}
function g_ {
    chromium-browser "http://www.google.com/search?hl=en&q=$(rawurlencode $*)"
}
function w_ {
    chromium-browser "http://en.wikipedia.org/wiki/$(rawurlencode $*)"
}
function yv_ {
    chromium-browser "http://video.search.yahoo.com/search/video?p=\
$(rawurlencode $*)"
}
function yi_ {
    chromium-browser "http://images.search.yahoo.com/search/images?p=\
$(rawurlencode $*)"
}
function y_ {
    chromium-browser "http://search.yahoo.com/search?p=$(rawurlencode $*)"
}
function imdb_ {
    chromium-browser "http://www.imdb.com/find?q=$(rawurlencode $*)"
}
function h_ {
    chromium-browser "http://www.hyperdictionary.com/search.aspx?define=\
$(rawurlencode $*)"
}
function v_ {
    chromium-browser "http://www.vandale.nl/opzoeken?pattern=$(rawurlencode $*)\
&lang=nn"
}
function yt_ {
    chromium-browser "http://www.youtube.com/results?search_query=\
$(rawurlencode $*)"
}
function gs_ {
    chromium-browser "http://scholar.google.nl/scholar?hl=nl&lr=&q=\
$(rawurlencode $*)"
}
function gv_ {
    chromium-browser "http://video.google.com/videosearch?hl=en&q=\
$(rawurlencode $*)"
}
function gi_ {
    chromium-browser "http://images.google.com/images?q=$(rawurlencode $*)"
}
function gnl_ {
    chromium-browser "http://www.google.nl/search?hl=nl&q=$(rawurlencode $*)"
}
function wa_ {
	chromium-browser "http://www.wolframalpha.com/input/?i=$(rawurlencode $*)"
}

# note this function needs to be called as root
function install_launcher {
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ni_ \$\*"    > /usr/local/bin/i    && chmod +x /usr/local/bin/i
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ng_ \$\*"    > /usr/local/bin/g    && chmod +x /usr/local/bin/g
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nw_ \$\*"    > /usr/local/bin/wiki && chmod +x /usr/local/bin/wiki
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nyv_ \$\*"   > /usr/local/bin/yv   && chmod +x /usr/local/bin/yv
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nyi_ \$\*"   > /usr/local/bin/yi   && chmod +x /usr/local/bin/yi
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ny_ \$\*"    > /usr/local/bin/y    && chmod +x /usr/local/bin/y
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nimdb_ \$\*" > /usr/local/bin/imdb && chmod +x /usr/local/bin/imdb
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nh_ \$\*"    > /usr/local/bin/h    && chmod +x /usr/local/bin/h
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nv_ \$\*"    > /usr/local/bin/v    && chmod +x /usr/local/bin/v
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nyt_ \$\*"   > /usr/local/bin/yt   && chmod +x /usr/local/bin/yt
#    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ngs_ \$\*"   > /usr/local/bin/gs   && chmod +x /usr/local/bin/gs
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ngv_ \$\*"   > /usr/local/bin/gv   && chmod +x /usr/local/bin/gv
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ngi_ \$\*"   > /usr/local/bin/gi   && chmod +x /usr/local/bin/gi
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\ngnl_ \$\*"  > /usr/local/bin/gnl  && chmod +x /usr/local/bin/gnl
    printf "#!/bin/ksh93\n\n. ~/.kshrc\n\nwa_ \$\*"   > /usr/local/bin/wa   && chmod +x /usr/local/bin/wa
}

################################################################################
# untarring fixes
################################################################################

tar_=$(which tar)

if [[ "$tar_" ]]; then
function tar__FUCKIT
{
	if [[ "$1" = *x* ]]; then
		if [[ $($tar_ -tf $2 | sed 's/\/.*//g'|sort|uniq|wc -l) -gt 1 ]]; then
			dirnam=$(printf "$2"|sed "s/\.gz//g;s/\.tar//g;s/\.tgz//g")
			dirnam=$(basename $dirnam)

			if [[ "$2" = \.* ]]; then
				tarfile="${PWD}/$2"
				tarfile="$(dirname $tarfile)/$(basename $tarfile)"
			else
				tarfile="$2"
			fi

			printf "-----------------------------------------------------------------------------------------\n";
			printf "preventing extract of multiple files in current path, extracting to:\n";
			printf "created directory: $dirnam\n";
			printf "-----------------------------------------------------------------------------------------\n";

			if [[ -f "$dirnam" ]] || [[ -d "$dirnam" ]]; then
				printf "Directory (or file) $dirnam already exists. Won't continue\n"
				return
			fi

			(mkdir $dirnam
			 cd $dirnam
			 $tar_ "$1" "$tarfile")

			return
		fi
	fi
	$tar_ $*
}
fi

################################################################################
# hexcomp function for binary comparison
################################################################################

function hexcomp
{
	if ! [[ "$(which meld)" ]]; then
		print "meld not installed for diffing between $1 and $2"
	else
		TEMPFILE1=/tmp/kshrc_left_$$
		TEMPFILE2=/tmp/kshrc_right_$$

		trap "rm -f $TMPFILE1" EXIT HUP INT QUIT TERM
		trap "rm -f $TMPFILE2" EXIT HUP INT QUIT TERM

		hexdump -v -C $1 > $TEMPFILE1
		hexdump -v -C $2 > $TEMPFILE2

		meld $TEMPFILE1 $TEMPFILE2
	fi
}

################################################################################
# spawn and kill
################################################################################

killtree() {
    local _pid=$1
    local _sig=${2:--TERM}
    kill -stop ${_pid} # needed to stop quickly forking parent from producing children between child killing and parent killing
    for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
        killtree ${_child} ${_sig}
    done
    kill -${_sig} ${_pid}
}

typeset spawned=$!
function cleanup
{
    printf "\r\nCleaning up!\r\n"
    #echo kill -9 $spawned
    #kill -9 $spawned
    #exit 0
    # better way to terminate..
#    while read -d' ' pid; do
#        if ! [[ "$pid" == "" ]]; then
#            killtree $pid 15
#        fi
#    done < <(echo " $spawned ")
#    sleep 3 # although there is a race condition.. sometimes kill 15 isn't enough. :-(
    echo in cleanup..
    while read -d' ' pid; do
        if ! [[ "$pid" == "" ]]; then
            echo killtree $pid 9
            killtree $pid 9
        fi
    done < <(echo " $spawned ")
    echo kill -9 $spawned
    kill -9 $spawned
    exit 0
}
function launch
{
    trap "cleanup" 2
    ("$@")&
    spawned="$spawned $!"

    echo launched.. $spawned
}

function wait_
{
    while true; do
        echo Press Control+C to terminate running processes..
        jobs -l
        sleep 2
    done
}

# Example usage in a script:
#
#    function start_foo
#    {
#        while true; do
#            echo something...
#        done
#    }
#    launch start_foo
#    wait

################################################################################
# local overrules
################################################################################

if [[ -f "$HOME/.kshrc.local" ]]; then
 	. $HOME/.kshrc.local
fi

#printf '\33]50;%s\007' "xft:Menlo:pixelsize=16"

# Fix keyboard layout if it's screwed up
#setxkbmap -layout us

export TERM=xterm
#export TERM=xterm-256color

export PATH=$PATH:$HOME/bin/
export PATH=$PATH:$HOME/.bin/
export PATH=$PATH:$HOME/go/bin/
export PATH=$PATH:$HOME/.local/bin/
export PATH=$PATH:$HOME/superprofile/thunderbird/
export PATH=$PATH:$HOME/git-helpers/bin/

alias r="fc -e -"

alias top="top -d 0.5 || top -d 0,5"

function dim2 {
  xrandr|grep ' connected'|cut -d ' ' -f 1| xargs -I{} xrandr --output {} --brightness $1
  echo $1 > /var/tmp/current.dim
}
function dim {
  xrandr|grep ' connected'|cut -d ' ' -f 1| xargs -I{} xrandr --output {} --brightness 0.66667
  echo 0.66667 > /var/tmp/current.dim
}

function undim {
  xrandr|grep ' connected'|cut -d ' ' -f 1| xargs -I{} xrandr --output {} --brightness 1.0
  echo 1.0 > /var/tmp/current.dim
}

alias goto='. ~/.bin/goto.sh'

#[ -z "$TMUX" ] && export TERM=xterm-256color

if [[ -f /projects/nc11nt/profile.ksh ]]; then
. /projects/nc11nt/profile.ksh
fi

export GOPATH=$HOME
export PATH=$PATH:$HOME/go/bin
