# -*- coding: utf-8 -*-
from ranger.api.commands import *
import os
from os.path import expanduser

def execute_all(self, filename):
    lines = []
    home = expanduser("~")
    del_file = '{}/{}'.format(home, filename)
    if os.path.exists(del_file):
        with open(del_file) as data_file:
            data = data_file.read()
            lines = data.split("\n")

    file = open(del_file, "a+")
    for f in self.fm.thistab.get_selection():
        if not f.realpath in lines:
            file.write(f.realpath + "\n")
    file.close()
    self.fm.notify("Added selection to: {}".format(del_file))

class queue_deletion(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_delete.txt')

class queue_picture(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_pictures.txt')

class queue_movie(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_movies.txt')

class queue_music(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_music.txt')

class queue_note(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_notes.txt')

class queue_script(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_scripts.txt')

class queue_personal(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_personal.txt')

class queue_document(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_documents.txt')

class queue_project(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_projects.txt')

class queue_3dprint(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_3dprinting.txt')

class queue_hide(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_hidden.txt')

class queue_application(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_applications.txt')

class queue_backup(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_backups.txt')

class queue_graphic(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_graphics.txt')

class queue_data(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_data.txt')

class queue_home(Command):
    def execute(self):
        return execute_all(self, 'ranger_queued_home.txt')
