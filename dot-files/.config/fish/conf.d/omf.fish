# Path to Oh My Fish install.
set -q XDG_DATA_HOME
  and set -gx OMF_PATH "$XDG_DATA_HOME/omf"
  or set -gx OMF_PATH "$HOME/.local/share/omf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish

# Added (Ray)
function fish_mode_prompt --description 'Displays the current mode'
	# Do nothing if not in vi mode
	if test "$fish_key_bindings" = "fish_vi_key_bindings"
		switch $fish_bind_mode
			case default
				set_color --bold yellow
				echo "-- COMMAND -- "
			case insert
				# set_color --bold green
				# echo "-- INSERT -- "
			case replace-one
				set_color --bold red
				echo "-- REPLACE -- "
			case visual
				set_color --bold brmagenta
				echo "-- VISUAL -- "
		end
		set_color normal
	end
end

