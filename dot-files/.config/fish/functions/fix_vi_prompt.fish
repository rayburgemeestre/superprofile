function fish_mode_prompt --description 'Displays the current mode'
	# Do nothing if not in vi mode
	if test "$fish_key_bindings" = "fish_vi_key_bindings"
		switch $fish_bind_mode
			case default
				set_color --bold yellow
				echo "-- COMMAND -- "
			case insert
				# set_color --bold green
				# echo "-- INSERT -- "
			case replace-one
				set_color --bold red
				echo "-- REPLACE -- "
			case visual
				set_color --bold brmagenta
				echo "-- VISUAL -- "
		end
		set_color normal
	end
end

