#!/bin/ksh93

sudo apt-get install    php-cli vim rxvt-unicode-256color ansible ksh \
                        xclip nfs-common cmake \
                        expect openvpn  \
                        virtualbox gparted screen numlockx \
                        lxappearance dmz-cursor-theme

sudo apt-get install -y feh

# NOTE:
# dmz-cursor-theme = one random result from "apt search cursor | grep theme"
# run lxappearance to set a cursor theme! you have to reset and reopen windows for it to take effect
